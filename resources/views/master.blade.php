<!DOCTYPE html>
<html lang="en">
<!-- Copied from http://pixner.net/dooplo/dooplo/index.html by Cyotek WebCopy 1.7.0.600, Sunday, October 6, 2019, 12:49:24 PM -->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="forntEnd-Developer" content="Mamunur Rashid">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title> GameDepan.GG - Komunitas Review Ribuan Game</title>
	<!-- favicon -->
	<link rel="shortcut icon" href="{{ asset('/assets/images/favicon.png')}}" type="image/x-icon">
	<!-- bootstrap -->
	<link rel="stylesheet" href="{{ asset('/assets/css/bootstrap.min.css')}}">
	<!-- Plugin css -->
	<link rel="stylesheet" href="{{ asset('/assets/css/plugin.css')}}">

	<!-- stylesheet -->
	<link rel="stylesheet" href="{{ asset('/assets/css/style.css')}}">
	<!-- responsive -->
	<link rel="stylesheet" href="{{ asset('/assets/css/responsive.css')}}">
	<!-- AppBlade -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">

	<style>
		.myDropdown {
		  position: relative;
		  display: inline-block;
		  color: white;
		}
		
		.myDropdown-content {
		  display: none;
		  position: absolute;
		  background-color: #f9f9f9;
		  min-width: 80px;
		  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
		  padding: 12px 16px;
		  z-index: 1;
		  right: 0;
		  font-size: small;
		}
		
		.myDropdown:hover .myDropdown-content {
		  display: block;
		}
		</style>


	<style>
		body {
			padding-right: 0px !important;
		}
	</style>

</head>

<body>
	<!-- preloader area start -->
	<div class="preloader" id="preloader">
		<div class="loader loader-1">
			<div class="loader-outter"></div>
			<div class="loader-inner"></div>
		</div>
	</div>
	<!-- preloader area end -->

	<!-- Header Area Start  -->
	<header class="header">
		<!-- Top Header Area Start -->
		<section class="top-header">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="content">
							<div class="left-content">
								<ul class="left-list">
									<li>
										<p>
											<i class="fas fa-globe"></i> <a href="http://sanbercode.com" target="_blank"
												rel="noopener noreferrer">sanbercode</a>
										</p>
									</li>
								</ul>
							</div>
							<div class="right-content">
								<ul class="right-list">
									@guest
									<li>
										<a href="#" class="sign-in" data-toggle="modal" data-target="#login">
											<i class="fas fa-user"></i> Sign In
										</a>
									</li>
									@else
										<li>
											<div class="myDropdown">
												<i class="fas fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
												<div class="myDropdown-content">
												<a href="{{ route('logout')}}" onclick="event.preventDefault();
												document.getElementById('logout-form').submit();">Log Out</a>

												<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
												@csrf
												</form>
												</div>
											</div>
									</li>
									@endguest
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Top Header Area End -->
		<!--Main-Menu Area Start-->
		<div class="mainmenu-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<nav class="navbar navbar-expand-lg navbar-light">
							<a class="navbar-brand text-white" href="index.html">
								<img src="{{asset('assets/images/logo.png')}}" alt="" srcset="">
							</a>
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_menu"
								aria-controls="main_menu" aria-expanded="false" aria-label="Toggle navigation">
								<span class="navbar-toggler-icon"></span>
							</button>
							<div class="collapse navbar-collapse fixed-height" id="main_menu">
								<ul class="navbar-nav ml-auto">
									<li class="nav-item">
										<a class="nav-link active" href="play.html">Home
											<div class="mr-hover-effect"></div></a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="play.html">Reviews
											<div class="mr-hover-effect"></div></a>
									</li>
									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle" href="#" role="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											Categories
											<div class="mr-hover-effect"></div>
										</a>
										<ul class="dropdown-menu">
											<li><a class="dropdown-item" href="about.html"> <i
														class="fa fa-angle-double-right"></i>Racing</a></li>
											<li><a class="dropdown-item" href="affiliate.html"> <i
														class="fa fa-angle-double-right"></i>Shooters</a></li>
											<li><a class="dropdown-item" href="awards.html"> <i
														class="fa fa-angle-double-right"></i>Strategy</a></li>
											<li><a class="dropdown-item" href="bonus.html"> <i
														class="fa fa-angle-double-right"></i>Online</a></li>
										</ul>
									</li>
									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle" href="#" role="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											Popular Games
											<div class="mr-hover-effect"></div>
										</a>
										<ul class="dropdown-menu">
											<li><a class="dropdown-item" href="about.html"> <i
														class="fa fa-angle-double-right"></i>CS:GO</a></li>
											<li><a class="dropdown-item" href="affiliate.html"> <i
														class="fa fa-angle-double-right"></i>Dota 2</a></li>
											<li><a class="dropdown-item" href="awards.html"> <i
														class="fa fa-angle-double-right"></i>Among Us</a></li>
											<li><a class="dropdown-item" href="bonus.html"> <i
														class="fa fa-angle-double-right"></i>Minecraft</a></li>
											<li><a class="dropdown-item" href="cart.html"> <i
														class="fa fa-angle-double-right"></i>Valorant</a></li>
										</ul>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="https://gitlab.com/Ztrohub/gamedepangang"
											target="_blank" rel="noopener noreferrer">Gitlab
											<div class="mr-hover-effect"></div></a>
									</li>
								</ul>
								@guest
								<a href="#" class="mybtn1" data-toggle="modal" data-target="#signin"> Register</a>
								@else
								<a href="#" class="mybtn1" data-toggle="modal" data-target="#signin"> Buat Review</a>
								@endguest
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<!--Main-Menu Area Start-->
	</header>
	<!-- Header Area End  -->

	<!-- Hero Area Start -->
	<div class="hero-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 d-flex align-self-center">
					<div class="left-content">
						<div class="content">
							<h5 class="subtitle">
								Ganteng Doang Main
							</h5>
							<h1 class="title">
								Game <br> Depan.GG
							</h1>
							<p class="text">
								Ayo join dan review ribuan game bersama komunitas terbesar sekarang juga!
							</p>
							<div class="links">
								@guest
								<a href="#" class="mybtn1 link1" data-toggle="modal" data-target="#signin">Join
									Sekarang</a>
								@else
								<a href="#" class="mybtn1">Join
									Sekarang</a>
								@endguest
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-7">
					<div class="hero-img2 d-block d-md-none">
						<img src="{{ asset('/assets/images/heroarea.png')}}" alt="">
					</div>
					<div class="hero-img d-none d-md-block">
						<img class="img-fluid full-image" src="{{ asset('/assets/images/heroarea.png')}}" alt="">
						<img class="shape phone" src="{{ asset('/assets/images/h-shapes/phone.png')}}" alt="">
						<img class="shape man" src="{{ asset('/assets/images/h-shapes/man222.png')}}" alt="">
						<img class="shape ripple" src="{{ asset('/assets/images/h-shapes/ripple.png')}}" alt="">
						<img class="shape ripple2" src="{{ asset('/assets/images/h-shapes/ripple1.png')}}" alt="">
						<img class="shape bitcoin2" src="{{ asset('/assets/images/h-shapes/bitcoin2.png')}}" alt="">
						<img class="shape shape-icon" src="{{ asset('/assets/images/h-shapes/shape.png')}}" alt="">
						<img class="shape award-bg" src="{{ asset('/assets/images/h-shapes/award/bg.png')}}" alt="">
						<img class="shape award" src="{{ asset('/assets/images/h-shapes/award/award.png')}}" alt="">
						<img class="shape gift-bg" src="{{ asset('/assets/images/h-shapes/giftbox/bg.png')}}" alt="">
						<img class="shape gift" src="{{ asset('/assets/images/h-shapes/giftbox/gift.png')}}" alt="">
						<img class="shape shield-bg" src="{{ asset('/assets/images/h-shapes/shield/bg.png')}}" alt="">
						<img class="shape shield" src="{{ asset('/assets/images/h-shapes/shield/shield.png')}}" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Hero Area End -->

	<!-- Featured Game Area Start -->
	<section class="featured-game">
		<!-- Features Area Start -->
		<div class="features">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="feature-box">
							<div class="feature-box-inner">
								<div class="row">
									<div class="col-lg-4 col-md-6">
										<div class="single-feature">
											<div class="icon one">
												<img src="assets/images/feature/icon1.png" alt="">
											</div>
											<div class="content">
												<h4 class="title">
													CS:GO
												</h4>
												<a href="#" class="link">telusuri review <i
														class="fas fa-arrow-right"></i></a>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-6">
										<div class="single-feature">
											<div class="icon two">
												<img src="assets/images/feature/icon2.png" alt="">
											</div>
											<div class="content">
												<h4 class="title">
													Dota 2
												</h4>
												<a href="#" class="link">telusuri review <i
														class="fas fa-arrow-right"></i></a>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-6">
										<div class="single-feature">
											<div class="icon three">
												<img src="assets/images/feature/icon3.png" alt="">
											</div>
											<div class="content">
												<h4 class="title">
													Among Us
												</h4>
												<a href="#" class="link">telusuri review <i
														class="fas fa-arrow-right"></i></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Features Area End -->
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8 col-md-10">
					<div class="section-heading">
						<h5 class="subtitle">
							Review bersama komunitas
						</h5>
						<h2 class="title">
							ribuan game baru
						</h2>
						<p class="text">
							Temukan ribuan review ditulis oleh komunitas game terbesar di Indonesia! Dan kamu bisa jadi
							salah satunya!
						</p>
					</div>
				</div>
			</div>
			<!-- Fun fact Area Start -->
			<div class="funfact">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<div class="single-fun">
								<img src="assets/images/funfact/icon1.png" alt="">
								<div class="count-area">
									<div class="count">93K</div>
								</div>
								<p>
									Users
								</p>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="single-fun">
								<img src="assets/images/funfact/icon2.png" alt="">
								<div class="count-area">
									<div class="count">99+</div>
								</div>
								<p>
									Reviews
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Fun fact Area End -->
		</div>
	</section>
	<!-- Featured Game Area	End -->

	<!-- Footer Area Start -->
	<footer class="footer" id="footer">
		<div class="subscribe-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="subscribe-box">
							<form action="#">
								<div class="row">
									<div class="col-lg-12">
										<div class="heading-area">
											<h5 class="sub-title">
												Langganan ke GameDepan.GG
											</h5>
											<h4 class="title">
												Jadilah member gaming ganteng
											</h4>
										</div>
									</div>
									<div class="col-lg-3 col-4 d-flex align-self-center">
										<div class="icon">
											<img src="{{ asset('/assets/images/mail-box.png')}}" alt="">
										</div>
									</div>
									<div class="col-lg-6 col-8 d-flex align-self-center">
										<div class="form-area">
											<input type="text" placeholder="Masukkan Email Kamu">
										</div>
									</div>
									<div class="col-lg-3 d-flex align-self-center">
										<div class="button-area">
											<button class="mybtn1" type="submit">Langganan
												<span><i class="fas fa-paper-plane"></i></span>
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="copy-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-5">
						<div class="left-area">
							<p>Copyright © 2020. All Rights Reserved By <a href="https://gitlab.com/Ztrohub/">Lukas
									Budi</a>
							</p>
						</div>
					</div>
					<div class="col-lg-7">
						<ul class="copright-area-links">
							<li>
								<a href="#">Template Powered by Dooplo</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer Area End -->

	<!-- Back to Top Start -->
	<div class="bottomtotop">
		<i class="fas fa-chevron-right"></i>
	</div>
	<!-- Back to Top End -->

	<!-- Login Area Start -->
	<div class="modal fade login-modal" id="login" tabindex="-1" role="dialog" aria-labelledby="login"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<div class="modal-body">
					<div class="logo-area">
						<img class="logo" src="{{ asset('/assets/images/logo.png')}}" alt="">
					</div>
					<div class="header-area">
						<h4 class="title">Selamat datang Gamers!</h4>
						<p class="sub-title">Silahkan login di bawah ini.</p>
					</div>
					<div class="form-area">
						<form action="{{ route('login')}}" method="POST">
							@csrf
							<div class="form-group">
								<label for="login-input-email">Email*</label>
								<input type="email" name="email"
									class="input-field @error('email') is-invalid @enderror" id="login-input-email"
									placeholder="Masukkan email kamu" value="{{ old('email') }}" required
									autocomplete="email" autofocus>
								@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
							<div class="form-group">
								<label for="login-input-password">Password*</label>
								<input type="password" name="password"
									class="input-field @error('password') is-invalid @enderror"
									id="login-input-password" placeholder="Password" required
									autocomplete="current-password">
								@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
							<div class="form-group">
								<div class="box">
									<div class="left">
										<input type="checkbox" name="remember" class="check-box-field"
											id="input-save-password" {{ old('remember') ? 'checked' : '' }}>
										<label for="input-save-password">Remember Me</label>
									</div>
									@if (Route::has('password.request'))
									<div class="right">
										<a href="{{ route('password.request')}}">
											Lupa Password?
										</a>
									</div>
									@endif
								</div>
							</div>
							<div class="form-group">
								<button type="submit" class="mybtn1">Log In</button>
							</div>
						</form>
					</div>
					<div class="form-footer">
						<p>Belum punya akun?
							<a href="#" data-dismiss="modal" data-toggle="modal" data-target="#signin">Buat akun <i
									class="fas fa-angle-double-right"></i></a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Login Area End -->

	<!-- SignIn Area Start -->
	<div class="modal fade login-modal sign-in" id="signin" tabindex="-1" role="dialog" aria-labelledby="signin"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered " role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<div class="modal-body">
					<div class="logo-area">
						<img class="logo" src="{{ asset('/assets/images/logo.png')}}" alt="">
					</div>
					<div class="header-area">
						<h4 class="title">Gabung jadi gamers ganteng</h4>
						<p class="sub-title">Silahkan register di bawah ini.</p>
					</div>
					<div class="form-area">
						<form id="registerForm" method="POST">
							@csrf
							<div class="form-group">
								<label for="input-name">Nama*</label>
								<input type="text" class="input-field" name="name" id="input-name" placeholder="Masukkan nama kamu" value="{{ old('name') }}" autocomplete="name" autofocus required>
								<span class="invalid-feedback" role="alert" id="nameError">
									<strong></strong>
								</span>
							</div>
							<div class="form-group">
								<label for="input-email">Email*</label>
								<input type="email" name="email" value="{{ old('email') }}" required autocomplete="email" class="input-field" id="input-email"
									placeholder="Masukkan email kamu">
									<span class="invalid-feedback" role="alert" id="emailError">
										<strong></strong>
									</span>
							</div>
							<div class="form-group">
								<label for="input-password">Password*</label>
								<input type="password" name="password" required autocomplete="new-password" class="input-field" id="input-password"
									placeholder="Masukkan password kamu">
									<span class="invalid-feedback" role="alert" id="passwordError">
										<strong></strong>
									</span>
							</div>
							<div class="form-group">
								<label for="input-con-password">Konfirmasi Password*</label>
								<input type="password" class="input-field" name="password_confirmation" required autocomplete="new-password" id="input-con-password"
									placeholder="Konfirmasi password kamu">
							</div>
							<div class="form-group">
								<button type="submit" class="mybtn1">Register!</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- SignIn Area End -->

	<!-- jquery -->
	<script src="{{ asset('/assets/js/jquery.js')}}"></script>
	<!-- popper -->
	<script src="{{ asset('/assets/js/popper.min.js')}}"></script>
	<!-- bootstrap -->
	<script src="{{ asset('/assets/js/bootstrap.min.js')}}"></script>
	<!-- plugin js-->
	<script src="{{ asset('/assets/js/plugin.js')}}"></script>

	<!-- MpusemoverParallax JS-->
	<script src="{{ asset('/assets/js/TweenMax.js')}}"></script>
	<script src="{{ asset('/assets/js/mousemoveparallax.js')}}"></script>
	<!-- main -->
	<script src="{{ asset('/assets/js/main.js')}}"></script>

	<!-- App.blade -->
	<script src="{{ asset('js/app.js') }}" defer></script>

	@if($errors->has('email') || $errors->has('password'))
	<script>
		$(function() {
        $('#login').modal({
            show: true
        });
    });
	</script>
	@endif

		<script>
$(function () {
    $('#registerForm').submit(function (e) {
        e.preventDefault();
        let formData = $(this).serializeArray();
        $(".invalid-feedback").children("strong").text("");
        $("#registerForm input").removeClass("is-invalid");
        $.ajax({
            method: "POST",
            headers: {
                Accept: "application/json"
            },
            url: "{{ route('register') }}",
            data: formData,
            success: () => window.location.assign("/review"),
            error: (response) => {
                if(response.status === 422) {
                    let errors = response.responseJSON.errors;
                    Object.keys(errors).forEach(function (key) {
                        $("#" + key + "Input").addClass("is-invalid");
                        $("#" + key + "Error").children("strong").text(errors[key][0]);
                    });
                } else {
                    window.location.reload();
                }
            }
        })
    });
})
</script>
</body>

<!-- Copied from http://pixner.net/dooplo/dooplo/index.html by Cyotek WebCopy 1.7.0.600, Sunday, October 6, 2019, 12:49:24 PM -->

</html>